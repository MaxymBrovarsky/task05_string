package com.brom.epam.task05;

import com.brom.epam.task05.view.TextProcessorView;

public class Application {
  public static void main(String[] args) {
    new TextProcessorView().show();
  }
}
