package com.brom.epam.task05.controller;

public class RegExConstant {
  public static final String SENTENCE_REGEX = "([^?.!]+)([?.!])";
  public static final String SPACE_REGEX = "\\s+";
  public static final String DUPLICATES_REGEX = "\\b?(\\w+)\\b.*\\b(\\1)\\b?";
  public static final String START_WITH_VOWEL_REGEX = "^[aeyuoi][^\\s]*";
  public static final String START_WITH_CONSONANT_REGEX = "^[b-df-hj-np-tv-xz][^\\s]*";
  public static final String VOWEL_REGEX = "[aeyuio]{1}";
  public static final String CONSONANT_REGEX = "[b-df-hj-np-tv-xz]{1}";
}
