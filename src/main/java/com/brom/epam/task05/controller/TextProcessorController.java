package com.brom.epam.task05.controller;

import com.brom.epam.task05.model.Sentence;
import com.brom.epam.task05.model.Word;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface TextProcessorController {
  void readFileContent(String filePath) throws IOException;
  int getNumberOfSentencesWithDuplicates();
  List<Sentence> getSentencesOrderedByWordsIn();
  Optional<Word> findUniqueWordInTextThatIsPresentInFirstSentence();
  List<Word> findWordsInInterrogativeSentencesByLength(int length);
  void replaceFirstWordThatStartWithVowelWithTheLongestWord();
  List<Word> getWordsInAlphabeticalOrderByFirstLetter();
  List<Word> sortWordsByPercentsOfVowelsIn();
  List<Word> sortWordsWhichBeginsWithVowelByFirstConsonant();
  List<Word> sortWordsBySomeLetter(char charForSorting);
  Map<String, Map<Sentence, Integer>> countWordOccurrencesInEachSentence(List<String> wordsForSearch);
  List<String> deleteSubstringFromEachSentence(String startSequence, String endSequence);
  String deleteWordsThatStartsWithConsonant(int length);
  List<Word> sortWordsBySomeLetterDesc(char charForSorting);
  String findLongestPalindrome();
  List<Word> deleteNextOccurrencesOfFirstLetter();
  void replaceWordBySubstring(int length, int numberOfSentence, String replacement);
}
