package com.brom.epam.task05.controller;

import com.brom.epam.task05.exceptions.NoConsonantException;
import com.brom.epam.task05.model.PunctuationMark;
import com.brom.epam.task05.model.Sentence;
import com.brom.epam.task05.model.Text;
import com.brom.epam.task05.model.Word;
import com.brom.epam.task05.util.FileReader;
import com.brom.epam.task05.view.TextProcessorView;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TextProcessorControllerImpl implements TextProcessorController {
  private static final Logger logger =
      LogManager.getLogger(TextProcessorControllerImpl.class.getName());
  private TextProcessorView view;
  private Text model = new Text();

  @Override
  public void readFileContent(String filePath) throws IOException {
    String content = FileReader.readFromFile(filePath);
    content = content.replaceAll(RegExConstant.SPACE_REGEX, " ");
    Pattern sentencePattern = Pattern.compile(RegExConstant.SENTENCE_REGEX);
    Matcher matcher = sentencePattern.matcher(content);
    while (matcher.find()) {
      String words = matcher.group(1);
      String endOfSentenceMark = matcher.group(2);
      List<String> wordStringList = Arrays.asList(words.split("\\W"));
      List<Word> sentenceWord = new ArrayList<>();
      wordStringList.forEach(w -> {
        if (w.length() != 0) {
          Word word = new Word(w);
          sentenceWord.add(word);
          model.addWord(word);
        }
      });
      Sentence sentence = new Sentence(sentenceWord, new PunctuationMark(endOfSentenceMark));
      sentence.setStringRepresentation(matcher.group(0));
      model.addSentence(sentence);

    }
  }

  public List<Sentence> getSentencesOrderedByWordsIn() {
    List<Sentence> sentences = this.model.getSentences();
    Comparator<Sentence> comparator = (s1, s2) -> {
      Integer size1 = s1.getWords().size();
      Integer size2 = s2.getWords().size();
      return size1.compareTo(size2);
    };
    Collections.sort(sentences, comparator);
    return sentences;
  }

  @Override
  public int getNumberOfSentencesWithDuplicates() {
    int count = 0;
    List<Sentence> sentences = this.model.getSentences();
    for (int i = 0; i < sentences.size(); i++) {
      Pattern pattern = Pattern.compile(RegExConstant.DUPLICATES_REGEX);
      Matcher matcher = pattern.matcher(sentences.get(i).toString());
      if (matcher.find()) {
        count++;
      }
    }
    return count;
  }

  @Override
  public Optional<Word> findUniqueWordInTextThatIsPresentInFirstSentence() {
    List<Sentence> sentences = model.getSentences();
    List<Word> wordsInFirstSentence = sentences.get(0).getWords();
    List<Sentence> sentencesWithoutFirst = new ArrayList<>(sentences);
    sentencesWithoutFirst.remove(0);
    List<Word> uniqueWords = wordsInFirstSentence.stream()
        .filter(w -> {
          return !sentencesWithoutFirst.stream().anyMatch(sentence -> {
            return sentence.getWords().contains(w);
          });
        }).collect(Collectors.toList());

    if (uniqueWords.isEmpty()) {
      return Optional.empty();
    } else {
      return Optional.of(uniqueWords.get(0));
    }
  }

  @Override
  public List<Word> findWordsInInterrogativeSentencesByLength(int length) {
    List<Word> words = new ArrayList<>();
    this.model.getSentences().stream()
        .filter(s -> s.isInterrogative())
        .forEach(s -> {
          s.getWords().forEach(w -> {
            if (w.getContent().length() == length) {
              words.add(w);
            }
          });
        });
    return words;
  }

  @Override
  public void replaceFirstWordThatStartWithVowelWithTheLongestWord() {
    List<Sentence> sentences = this.model.getSentences();
    Optional<Word> longestWord = this.findLongestWord();
    if (longestWord.isPresent()) {
      sentences.forEach(s -> {
        List<Word> words = s.getWords();
        for (int i = 0; i < words.size(); i++) {
          if (words.get(i).getContent().matches(RegExConstant.START_WITH_VOWEL_REGEX)) {
            logger.info(words.get(i).toString());
            words.set(i, longestWord.get());
          }
        }
      });
    } else {
      view.showError("No longest word is found");
    }
  }

  private Optional<Word> findLongestWord() {
    List<Sentence> sentences = this.model.getSentences();
    int maxLength = -1;
    Word longestWord = null;
    for (int i = 0; i < sentences.size(); i++) {
      List<Word> words = sentences.get(i).getWords();
      for (int j = 0; j < words.size(); j++) {
        if (words.get(j).getContent().length() > maxLength) {
          maxLength = words.get(j).getContent().length();
          longestWord = words.get(j);
        }
      }
    }
    return Optional.ofNullable(longestWord);
  }

  @Override
  public List<Word> getWordsInAlphabeticalOrderByFirstLetter() {
    List<Word> words = this.model.getWords().subList(0, this.model.getWords().size());
    Comparator<Word> wordComparator = Comparator.comparingInt(w -> w.getContent().charAt(0));
    words.sort(wordComparator);
    return words;
  }

  @Override
  public List<Word> sortWordsByPercentsOfVowelsIn() {
    List<Word> words = new ArrayList<>(this.model.getWords());
    Comparator<Word> wordComparator = Comparator.comparingDouble(w -> w.getPercentOfVowels());
    words.sort(wordComparator);
    return words;
  }

  @Override
  public List<Word> sortWordsWhichBeginsWithVowelByFirstConsonant() {
    List<Word> words = this.model.getWords().stream()
        .filter(w -> w.getContent().matches(RegExConstant.START_WITH_VOWEL_REGEX))
        .collect(Collectors.toList());
    Comparator<Word> comparator = Comparator.comparingInt(w -> {
      try {
        return w.getFirstConsonantCharCode();
      } catch (NoConsonantException e) {
        return -1;
      }
    });
    words.sort(comparator);
    return words;
  }

  @Override
  public List<Word> sortWordsBySomeLetter(char charForSorting) {
    List<Word> words = this.model.getWords();
    Comparator<Word> comparator =
        Comparator.comparing((Word w) -> w.getNumberOfCharacters(charForSorting))
        .thenComparing(w -> w.getContent());
    words.sort(comparator);
    return words;
  }

  @Override
  public Map<String, Map<Sentence, Integer>> countWordOccurrencesInEachSentence(
      List<String> wordsForSearch) {
    Map<String, Map<Sentence, Integer>> result = new HashMap<>();
    wordsForSearch.forEach(searchWord -> {
      Map<Sentence, Integer> sentenceToSearchWordCount = new HashMap<>();
      this.model.getSentences().forEach(s -> {
        int count = 0;
        sentenceToSearchWordCount.put(s, 0);
        s.getWords().forEach(w -> {
          if (w.getContent().equals(searchWord)) {
            int c = sentenceToSearchWordCount.get(s).intValue();
            sentenceToSearchWordCount.put(s, c+1);
          }
        });
        result.put(searchWord, sentenceToSearchWordCount);
      });
    });
    return result;
  }

  @Override
  public List<String> deleteSubstringFromEachSentence(String startSequence, String endSequence) {
    List<String> sentencesAfterRemovingSubstring = new ArrayList<>();
    String regExForReplacing = startSequence + "[^\\s]*" + endSequence;
    model.getSentences().forEach(s -> {
      sentencesAfterRemovingSubstring
          .add(s.getStringRepresentation().replaceAll(regExForReplacing, ""));
    });
    return sentencesAfterRemovingSubstring;
  }

  @Override
  public String deleteWordsThatStartsWithConsonant(int length) {
    List<Word> words = model.getWords();
    List<Word> wordsForDeletion = words.stream()
        .filter(w -> {
          String c = w.getContent();
          return c.matches(RegExConstant.START_WITH_CONSONANT_REGEX) && c.length() == length;
        })
        .collect(Collectors.toList());
    List<Word> w = new ArrayList<>(words);
    w.removeAll(wordsForDeletion);
    return w.stream().map(word -> word.getContent()).collect(Collectors.joining(" "));
  }

  @Override
  public List<Word> sortWordsBySomeLetterDesc(char charForSorting) {
    List<Word> words = model.getWords();
    Comparator<Word> comparator =
        Comparator.comparing((Word w) -> w.getNumberOfCharacters(charForSorting)).reversed()
            .thenComparing(w -> w.getContent());
    words.sort(comparator);
    return words;
  }

  public String findLongestPalindrome() {
    logger.debug(model);
    return this.findLongestPalindrome(model.toString());
  }

  private String findLongestPalindrome(String s) {
    if (s.isEmpty()) {
      return "Please enter a String";
    }
    if (s.length() == 1) {
      return s;
    }
    String longest = s.substring(0, 1);
    for (int i = 0; i < s.length(); i++) {
      String tmp = checkForEquality(s, i, i);
      if (tmp.length() > longest.length()) {
        longest = tmp;
      }
      tmp = checkForEquality(s, i, i + 1);
      if (tmp.length() > longest.length()) {
        longest = tmp;
      }
    }

    return longest;
  }

  private String checkForEquality(String s, int begin, int end) {
    while (begin >= 0 && end <= s.length() - 1 && s.charAt(begin) == s.charAt(end)) {
      begin--;
      end++;
    }
    return s.substring(begin + 1, end);
  }

  @Override
  public List<Word> deleteNextOccurrencesOfFirstLetter() {
    List<Word> words = model.getWords();
    List<Word> result = new ArrayList<>();
    words.stream().forEach(word -> {
      String firstSymbol = word.getContent().substring(0,1);
      word.getContent().replaceAll(firstSymbol, "");
      result.add(new Word(firstSymbol + word.getContent().replaceAll(firstSymbol, "")));
    });
    return result;
  }

  @Override
  public void replaceWordBySubstring(int length, int numberOfSentence, String replacement) {
    List<Word> wordsInSentence = this.model.getSentences().get(numberOfSentence - 1).getWords();
    for (int i = 0; i < wordsInSentence.size(); i++) {
      if (wordsInSentence.get(i).getContent().length() == length) {
        wordsInSentence.get(i).setContent(replacement);
      }
    }
  }
}
