package com.brom.epam.task05.model;

public class PunctuationMark {
  private String content;

  public PunctuationMark(String content) {
    this.content = content;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  @Override
  public boolean equals(Object obj) {
    return this.content.equals(obj);
  }
}
