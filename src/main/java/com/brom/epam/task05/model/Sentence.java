package com.brom.epam.task05.model;

import java.util.List;
import java.util.stream.Collectors;

public class Sentence {
  private List<Word> words;
  private PunctuationMark endOfSentence;
  private String stringRepresentation;

  public Sentence(List<Word> words, PunctuationMark endOfSentence) {
    this.words = words;
    this.endOfSentence = endOfSentence;
  }

  public String getStringRepresentation() {
    return stringRepresentation;
  }

  public void setStringRepresentation(String stringRepresentation) {
    this.stringRepresentation = stringRepresentation;
  }

  public List<Word> getWords() {
    return words;
  }

  public void setWords(List<Word> words) {
    this.words = words;
  }

  @Override
  public String toString() {
    return words.stream().map(Word::toString).collect(Collectors.joining(" "));
  }

  public boolean isInterrogative() {
    return this.endOfSentence.equals("?");
  }
}
