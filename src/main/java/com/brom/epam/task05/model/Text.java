package com.brom.epam.task05.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Text {
  private List<Sentence> sentences = new ArrayList<>();
  private List<Word> words = new ArrayList<>();

  public List<Sentence> getSentences() {
    return sentences;
  }

  public void setSentences(List<Sentence> sentences) {
    this.sentences = sentences;
  }

  public void addWord(Word word) {
    this.words.add(word);
  }

  public void addSentence(Sentence sentence) {
    this.sentences.add(sentence);
  }

  public List<Word> getWords() {
    return words;
  }

  @Override
  public String toString() {
    return sentences.stream().map(s -> s.toString()).collect(Collectors.joining(" "));
  }
}
