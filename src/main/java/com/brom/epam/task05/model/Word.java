package com.brom.epam.task05.model;

import com.brom.epam.task05.controller.RegExConstant;
import com.brom.epam.task05.exceptions.NoConsonantException;

public class Word {
  private String content;

  public Word(String content) {
    this.content = content;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public double getPercentOfVowels() {
    String[] chars = content.split("");
    int vowelCount = 0;
    for (int i = 0; i < chars.length; i++) {
      if (chars[i].matches(RegExConstant.VOWEL_REGEX)) {
        vowelCount++;
      }
    }
    return vowelCount * 100.0 / this.content.length();
  }

  public int getFirstConsonantCharCode() {
    String[] chars = content.split("");
    for (int i = 0; i < chars.length; i++) {
      if (chars[i].matches(RegExConstant.CONSONANT_REGEX)) {
        return chars[i].codePointAt(0);
      }
    }
    throw new NoConsonantException();
  }

  public int getNumberOfCharacters(char c) {
    int count = 0;
    char[] chars = this.content.toCharArray();
    for (int i = 0; i < chars.length; i++) {
      if (chars[i] == c) {
        count++;
      }
    }
    return count;
  }
  @Override
  public boolean equals(Object obj) {
    return this.getContent().equals(((Word)obj).content);
  }

  @Override
  public String toString() {
    return content;
  }
}
