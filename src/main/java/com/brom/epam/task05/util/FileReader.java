package com.brom.epam.task05.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public interface FileReader {
  static String readFromFile(String filePath) throws IOException {
    return new String(Files.readAllBytes(Paths.get(filePath)));
  }
}
