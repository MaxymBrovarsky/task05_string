package com.brom.epam.task05.view;

@FunctionalInterface
public interface Command {
    void execute();
}
