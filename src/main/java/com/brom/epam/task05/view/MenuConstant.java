package com.brom.epam.task05.view;

public class MenuConstant {
  public static final String RESOURCE_BUNDLE_NAME = "Menu";

  public static final String UKRAINIAN_LOCALE = "uk";
  public static final String ENGLISH_LOCALE = "en";
  public static final String CHINESE_LOCALE = "ch";
  public static final String JAPANESE_LOCALE = "jp";
  public static final String SPANISH_LOCALE = "sp";

  public static final String LOAD_FILE_KEY = "1";
  public static final String CHANGE_TO_UKRAINIAN_KEY = "2";
  public static final String CHANGE_TO_ENGLISH_KEY = "3";
  public static final String PRINT_NUMBER_OF_SENTENCES_WITH_DUPLICATES = "4";
  public static final String PRINT_SENTENCES_IN_ASC_BY_WORDS_KEY = "5";
  public static final String FIND_UNIQUE_WORDS_IN_FIRST_SENTENCE_KEY = "6";
  public static final String PRINT_WORDS_BY_LENGTH_KEY = "7";
  public static final String REPLACE_FIRST_WORD_WITH_THE_LONGEST_KEY = "8";
  public static final String PRINT_WORDS_IN_ALPHABETICAL_ORDER_KEY = "9";
  public static final String SORT_WORDS_BY_PERCENTS_OF_VOWELS_KEY = "10";
  public static final String SORT_WORDS_WHICH_BEGIN_WITH_VOWEL_BY_CONSONANT = "11";
  public static final String SORT_WORDS_BY_SOME_LETTER = "12";
  public static final String COUNT_WORD_OCCURRENCES_IN_EACH_SENTENCE = "13";
  public static final String DELETE_SUBSTRING_FROM_EACH_SENTENCE = "14";
  public static final String DELETE_WORDS_THAT_STARTS_WITH_CONSONANT = "15";
  public static final String SORT_WORDS_BY_SOME_LETTER_DESC = "16";
  public static final String FIND_THE_LONGEST_PALINDROME = "17";
  public static final String DELETE_NEXT_OCCURRENCES_OF_FIRST_LETTER_IN_WORDS = "18";
  public static final String CHANGE_SOME_WORD_IN_SOME_SENTENCE = "19";
  public static final String CHANGE_TO_CHINESE_KEY = "20";
  public static final String CHANGE_TO_JAPANESE_KEY = "21";
  public static final String CHANGE_TO_SPANISH_KEY = "22";
}
