package com.brom.epam.task05.view;

import com.brom.epam.task05.controller.TextProcessorController;
import com.brom.epam.task05.controller.TextProcessorControllerImpl;
import com.brom.epam.task05.model.Sentence;
import com.brom.epam.task05.model.Word;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TextProcessorView {
  private static final Logger logger = LogManager.getLogger(TextProcessorView.class.getName());
  private Map<String, Command> menuCommands;
  private Map<String, String> menu;
  private TextProcessorController controller;
  private ResourceBundle menuBundle;
  private Locale locale;
  private Scanner input;

  public TextProcessorView() {
    input = new Scanner(System.in);
    this.locale = new Locale(MenuConstant.ENGLISH_LOCALE);
    this.menuBundle = ResourceBundle.getBundle(MenuConstant.RESOURCE_BUNDLE_NAME, locale);
    this.controller = new TextProcessorControllerImpl();
    this.initMenu();
    this.loadMenuLocalization(locale);
  }

  private void initMenu() {
    this.menuCommands = new LinkedHashMap<>();
    menuCommands.put(MenuConstant.LOAD_FILE_KEY, this::loadFile);
    menuCommands.put(MenuConstant.CHANGE_TO_UKRAINIAN_KEY,
        this::changeLocalizationToUkrainian);
    menuCommands.put(MenuConstant.CHANGE_TO_ENGLISH_KEY,
        this::changeLocalizationToEnglish);
    menuCommands.put(MenuConstant.PRINT_SENTENCES_IN_ASC_BY_WORDS_KEY,
        this::printSentencesAscByWords);
    menuCommands.put(MenuConstant.PRINT_NUMBER_OF_SENTENCES_WITH_DUPLICATES,
        this::printNumberOfSentencesWithDuplicates);
    menuCommands.put(MenuConstant.PRINT_WORDS_BY_LENGTH_KEY,
        this::printWordsInInterrogativeSentencesByLength);
    menuCommands.put(MenuConstant.FIND_UNIQUE_WORDS_IN_FIRST_SENTENCE_KEY,
        this::printUniqueWordsInFirstSentenceThanAreUniqueInWholeText);
    menuCommands.put(MenuConstant.REPLACE_FIRST_WORD_WITH_THE_LONGEST_KEY,
        this::replaceFirstWordThatStartWithVowelWithTheLongestWord);
    menuCommands.put(MenuConstant.PRINT_WORDS_IN_ALPHABETICAL_ORDER_KEY,
        this::printWordsInAlphabeticalOrderByFirstLetter);
    menuCommands.put(MenuConstant.SORT_WORDS_BY_PERCENTS_OF_VOWELS_KEY,
        this::sortWordsByPercentsOfVowelsIn);
    menuCommands.put(MenuConstant.SORT_WORDS_WHICH_BEGIN_WITH_VOWEL_BY_CONSONANT,
        this::sortWordsWhichBeginsWithVowelByFirstConsonant);
    menuCommands.put(MenuConstant.SORT_WORDS_BY_SOME_LETTER,
        this::sortWordsBySomeLetter);
    menuCommands.put(MenuConstant.COUNT_WORD_OCCURRENCES_IN_EACH_SENTENCE,
        this::printWordOccurrencesInEachSentence);
    menuCommands.put(MenuConstant.DELETE_SUBSTRING_FROM_EACH_SENTENCE,
        this::deleteSubstringFromEachSentence);
    menuCommands.put(MenuConstant.DELETE_WORDS_THAT_STARTS_WITH_CONSONANT,
        this::deleteWordsThatStartsWithConsonant);
    menuCommands.put(MenuConstant.SORT_WORDS_BY_SOME_LETTER_DESC,
        this::sortWordsBySomeLetterDesc);
    menuCommands.put(MenuConstant.FIND_THE_LONGEST_PALINDROME,
        this::findTheLongestPalindrome);
    menuCommands.put(MenuConstant.DELETE_NEXT_OCCURRENCES_OF_FIRST_LETTER_IN_WORDS,
        this::deleteNextOccurrencesOfFirstLetter);
    menuCommands.put(MenuConstant.CHANGE_SOME_WORD_IN_SOME_SENTENCE,
        this::changeSomeWordInSomeSentence);
    menuCommands.put(MenuConstant.CHANGE_TO_CHINESE_KEY,
        this::changeLocalizationToChinese);
    menuCommands.put(MenuConstant.CHANGE_TO_JAPANESE_KEY,
        this::changeLocalizationToJapanese);
        menuCommands.put(MenuConstant.CHANGE_TO_SPANISH_KEY,
        this::changeLocalizationToSpanish);
  }

  public void loadMenuLocalization(Locale locale) {
    this.menuBundle = ResourceBundle.getBundle(MenuConstant.RESOURCE_BUNDLE_NAME, locale);
    this.menu = new LinkedHashMap<>();
    this.menu.put(MenuConstant.LOAD_FILE_KEY, menuBundle.getString(MenuConstant.LOAD_FILE_KEY));
    this.menu.put(MenuConstant.CHANGE_TO_UKRAINIAN_KEY,
        menuBundle.getString(MenuConstant.CHANGE_TO_UKRAINIAN_KEY));
    this.menu.put(MenuConstant.CHANGE_TO_ENGLISH_KEY,
        menuBundle.getString(MenuConstant.CHANGE_TO_ENGLISH_KEY));
    this.menu.put(MenuConstant.PRINT_NUMBER_OF_SENTENCES_WITH_DUPLICATES,
        menuBundle.getString(MenuConstant.PRINT_NUMBER_OF_SENTENCES_WITH_DUPLICATES));
    this.menu.put(MenuConstant.PRINT_SENTENCES_IN_ASC_BY_WORDS_KEY,
        menuBundle.getString(MenuConstant.PRINT_SENTENCES_IN_ASC_BY_WORDS_KEY));
    this.menu.put(MenuConstant.FIND_UNIQUE_WORDS_IN_FIRST_SENTENCE_KEY,
        menuBundle.getString(MenuConstant.FIND_UNIQUE_WORDS_IN_FIRST_SENTENCE_KEY));
    this.menu.put(MenuConstant.PRINT_WORDS_BY_LENGTH_KEY,
        menuBundle.getString(MenuConstant.PRINT_WORDS_BY_LENGTH_KEY));
    this.menu.put(MenuConstant.REPLACE_FIRST_WORD_WITH_THE_LONGEST_KEY,
        menuBundle.getString(MenuConstant.REPLACE_FIRST_WORD_WITH_THE_LONGEST_KEY));
    this.menu.put(MenuConstant.PRINT_WORDS_IN_ALPHABETICAL_ORDER_KEY,
        menuBundle.getString(MenuConstant.PRINT_WORDS_IN_ALPHABETICAL_ORDER_KEY));
    this.menu.put(MenuConstant.SORT_WORDS_BY_PERCENTS_OF_VOWELS_KEY,
        menuBundle.getString(MenuConstant.SORT_WORDS_BY_PERCENTS_OF_VOWELS_KEY));
    this.menu.put(MenuConstant.SORT_WORDS_WHICH_BEGIN_WITH_VOWEL_BY_CONSONANT,
        menuBundle.getString(MenuConstant.SORT_WORDS_WHICH_BEGIN_WITH_VOWEL_BY_CONSONANT));
    this.menu.put(MenuConstant.SORT_WORDS_BY_SOME_LETTER,
        menuBundle.getString(MenuConstant.SORT_WORDS_BY_SOME_LETTER));
    this.menu.put(MenuConstant.COUNT_WORD_OCCURRENCES_IN_EACH_SENTENCE,
        menuBundle.getString(MenuConstant.COUNT_WORD_OCCURRENCES_IN_EACH_SENTENCE));
    this.menu.put(MenuConstant.DELETE_SUBSTRING_FROM_EACH_SENTENCE,
        menuBundle.getString(MenuConstant.DELETE_SUBSTRING_FROM_EACH_SENTENCE));
    this.menu.put(MenuConstant.SORT_WORDS_BY_SOME_LETTER_DESC,
        menuBundle.getString(MenuConstant.SORT_WORDS_BY_SOME_LETTER_DESC));
    this.menu.put(MenuConstant.FIND_THE_LONGEST_PALINDROME,
        menuBundle.getString(MenuConstant.FIND_THE_LONGEST_PALINDROME));
    this.menu.put(MenuConstant.DELETE_NEXT_OCCURRENCES_OF_FIRST_LETTER_IN_WORDS,
        menuBundle.getString(MenuConstant.DELETE_NEXT_OCCURRENCES_OF_FIRST_LETTER_IN_WORDS));
    this.menu.put(MenuConstant.CHANGE_SOME_WORD_IN_SOME_SENTENCE,
        menuBundle.getString(MenuConstant.CHANGE_SOME_WORD_IN_SOME_SENTENCE));
    this.menu.put(MenuConstant.CHANGE_TO_CHINESE_KEY,
        menuBundle.getString(MenuConstant.CHANGE_TO_CHINESE_KEY));
    this.menu.put(MenuConstant.CHANGE_TO_JAPANESE_KEY,
        menuBundle.getString(MenuConstant.CHANGE_TO_JAPANESE_KEY));
    this.menu.put(MenuConstant.CHANGE_TO_SPANISH_KEY,
        menuBundle.getString(MenuConstant.CHANGE_TO_SPANISH_KEY));
  }

  public void show() {
    while (true) {
      menu.forEach((k, v) -> {
        logger.info(v);
      });
      String commandKey = input.nextLine();
      Command command = this.menuCommands.get(commandKey);
      if (command != null) {
        command.execute();
      }
    }
  }

  public void loadFile() {
    logger.info("Enter file path: ");
    String filePath = input.nextLine();
    try {
      controller.readFileContent(filePath);
    } catch (IOException e) {
      logger.error("Here are some error. Please try again");
    }
  }

  public void printSentencesAscByWords() {
    List<Sentence> sentences = this.controller.getSentencesOrderedByWordsIn();
    sentences.forEach(s -> {
      logger.info(s);
    });
  }

  public void printNumberOfSentencesWithDuplicates() {
    logger.info(this.controller.getNumberOfSentencesWithDuplicates());
  }

  public void printWordsInInterrogativeSentencesByLength() {
    logger.info("Please enter length of word");
    int length = input.nextInt();
    input.nextLine();
    List<Word> words = controller.findWordsInInterrogativeSentencesByLength(length);
    if (words.isEmpty()) {
      logger.info("Found zero words");
    } else {
      logger.info(words);
    }
  }

  public void printUniqueWordsInFirstSentenceThanAreUniqueInWholeText() {
    Optional<Word> uniqueWord = controller.findUniqueWordInTextThatIsPresentInFirstSentence();
    if (uniqueWord.isPresent()) {
      logger.info("unique word: " + uniqueWord.get());
    } else {
      logger.info("there no unique words");
    }
  }

  public void replaceFirstWordThatStartWithVowelWithTheLongestWord() {
    this.controller.replaceFirstWordThatStartWithVowelWithTheLongestWord();
  }

  public void printWordsInAlphabeticalOrderByFirstLetter() {
    List<Word> sortedWords = this.controller.getWordsInAlphabeticalOrderByFirstLetter();
    int tabsMarginMultiplier = 0;
    char startSymbol = sortedWords.get(0).getContent().charAt(0);
    for (int i = 0; i < sortedWords.size(); i++) {
      if (sortedWords.get(i).getContent().charAt(0) != startSymbol) {
        tabsMarginMultiplier++;
      }
      startSymbol = sortedWords.get(i).getContent().charAt(0);
      String margin = "";
      for (int j = 0; j < tabsMarginMultiplier; j++) {
        margin += "\t";
      }
      logger.info(margin + sortedWords.get(i).getContent());
    }
  }

  public void showError(String message) {
    logger.error("Error happen!!!\nmessage = " + message);
  }

  public void sortWordsByPercentsOfVowelsIn() {
    List<Word> words = this.controller.sortWordsByPercentsOfVowelsIn();
    words.forEach(w -> logger.info(w));
  }

  public void sortWordsWhichBeginsWithVowelByFirstConsonant() {
    List<Word> words = this.controller.sortWordsWhichBeginsWithVowelByFirstConsonant();
    words.forEach(w -> logger.info(w));
  }

  public void sortWordsBySomeLetter() {
    logger.info("Enter character for sorting");
    char charForSorting = input.next().charAt(0);
    input.nextLine();
    List<Word> words = this.controller.sortWordsBySomeLetter(charForSorting);
    words.forEach(w -> logger.info(w));
  }

  public void printWordOccurrencesInEachSentence() {
    logger.info("Enter word each in new line(finish input by empty line)");
    List<String> wordsForSearch = new ArrayList<>();
    while (true) {
      String wordForSearch = input.nextLine();
      if (wordForSearch.isEmpty()) {
        break;
      }
      wordsForSearch.add(wordForSearch);
    }
    Map<String, Map<Sentence, Integer>> map =
        controller.countWordOccurrencesInEachSentence(wordsForSearch);
    map.entrySet().forEach(entry -> {
      logger.info("---------Word: " + entry.getKey() + "-------------");
      entry.getValue().entrySet().forEach(e -> {
        logger.info("----------------------------------");
        logger.info("sentence = " + e.getKey());
        logger.info("occurrence of " + entry.getKey() + " = " + e.getValue());
      });
    });
  }

  public void deleteSubstringFromEachSentence() {
    logger.info("Enter start symbol");
    String startSequence = input.nextLine();
    logger.info("Enter end symbol");
    String endSequence = input.nextLine();
    List<String> sentences = this.controller
        .deleteSubstringFromEachSentence(startSequence, endSequence);
    sentences.forEach(s -> logger.info(s));
  }

  public void deleteWordsThatStartsWithConsonant() {
    logger.info("Please enter length of word");
    int length = input.nextInt();
    input.nextLine();
    logger.info(controller.deleteWordsThatStartsWithConsonant(length));
  }

  public void sortWordsBySomeLetterDesc() {
    logger.info("Enter character for sorting");
    char charForSorting = input.next().charAt(0);
    input.nextLine();
    List<Word> words = this.controller.sortWordsBySomeLetterDesc(charForSorting);
    words.forEach(w -> logger.info(w));
  }

  public void findTheLongestPalindrome() {
    logger.info("The longest palindrome = " + this.controller.findLongestPalindrome());
  }

  public void deleteNextOccurrencesOfFirstLetter() {
    List<Word> words = this.controller.deleteNextOccurrencesOfFirstLetter();
    words.forEach(w -> logger.info(w));
  }

  public void changeSomeWordInSomeSentence() {
    logger.info("Enter please number of sentence");
    int numberOfSentence = input.nextInt();
    input.nextLine();
    logger.info("Enter please length of word");
    int lengthOfWord = input.nextInt();
    input.nextLine();
    logger.info("Enter substring");
    String substring = input.nextLine();
    controller.replaceWordBySubstring(lengthOfWord, numberOfSentence,substring);
  }

  public void changeLocalizationToUkrainian() {
    locale = new Locale(MenuConstant.UKRAINIAN_LOCALE);
    this.loadMenuLocalization(locale);
  }

  public void changeLocalizationToEnglish() {
    locale = new Locale(MenuConstant.ENGLISH_LOCALE);
    this.loadMenuLocalization(locale);
  }

  public void changeLocalizationToChinese() {
    locale = new Locale(MenuConstant.CHINESE_LOCALE);
    this.loadMenuLocalization(locale);
  }

  public void changeLocalizationToJapanese() {
    locale = new Locale(MenuConstant.JAPANESE_LOCALE);
    this.loadMenuLocalization(locale);
  }

  public void changeLocalizationToSpanish() {
    locale = new Locale(MenuConstant.SPANISH_LOCALE);
    this.loadMenuLocalization(locale);
  }

}
